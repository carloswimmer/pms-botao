export default {
  tipo: {
    type: String,
    required: false,
    default: 'button'
  },
  
  estilo: {
    type: String,
    required: false,
    default: 'primario'
  },
  
  rotulo: {
    type: String,
    required: false,
    default: ''
  },
  
  icone: {
    type: String,
    required: false,
    default: ''
  },
  
  reverso: {
    type: Boolean,
    required: false,
    default: false
  },
  
  toggle: {
    default: '',
    type: String,
    required: false
  },
  
  target: {
    default: '',
    type: String,
    required: false
  },
  
  dismiss: {
    default: '',
    type: String,
    required: false
  }
}
