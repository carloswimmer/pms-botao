export const estilos = [
  { nome: 'secundario', classes: 'btn-primary' },
  { nome: 'primario', classes: 'btn-primary side-btn-primary' },
  { nome: 'secundario-leve', classes: 'btn-default' },
  { nome: 'desfaz', classes: 'btn-default side-btn-default' },
  { nome: 'secundario-dir', classes: 'btn-primary btn-next' },
  { nome: 'primario-dir', classes: 'btn-primary side-btn-primary btn-next' },
  { nome: 'secundario-leve-dir', classes: 'btn-default btn-next' },
  { nome: 'secundario-esq', classes: 'btn-primary btn-previous' },
  { nome: 'secundario-leve-esq', classes: 'btn-default btn-previous' },
  { nome: 'desfaz-esq', classes: 'btn-default side-btn-default btn-previous' },
  { nome: 'primario-esq', classes: 'btn-primary side-btn-primary btn-previous' },
  { nome: 'atencao-esq', classes: 'btn-warning btn-previous' },
  { nome: 'secundario-sm', classes: 'btn-primary btn-sm' },
  { nome: 'atencao-sm', classes: 'btn-warning btn-sm' },
  { nome: 'perigo-sm', classes: 'btn-danger btn-sm' },
  { nome: 'secundario-leve-sm', classes: 'btn-default btn-sm' },
  { nome: 'primario-lg', classes: 'btn-primary side-btn-primary btn-subscribe btn-lg' },
  { nome: 'secundario-lg', classes: 'btn-primary btn-subscribe btn-lg' },
  { nome: 'secundario-inline', classes: 'btn-primary btn-inline' }
]

export const icones = [
  { nome: 'check', classes: 'fa-check' },
  { nome: 'ban', classes: 'fa-ban' },
  { nome: 'file', classes: 'fa-file-o' },
  { nome: 'plus', classes: 'fa-plus' },
  { nome: 'floppy', classes: 'fa-floppy-o' },
  { nome: 'eraser', classes: 'fa-eraser' },
  { nome: 'trash', classes: 'fa-trash fa-lg' },
  { nome: 'pencil', classes: 'fa-pencil fa-lg' },
  { nome: 'print', classes: 'fa fa-print fa-lg' },
  { nome: 'caret-left', classes: 'fa-caret-left fa-lg' },
  { nome: 'caret-right', classes: 'fa-caret-right fa-lg' },
  { nome: 'search', classes: 'fa-search' },
  { nome: 'eye', classes: 'fa-eye fa-lg' },
  { nome: 'spinner', classes: 'fa-spinner fa-pulse' },
  { nome: 'power-off', classes: 'fa-power-off' }
]
