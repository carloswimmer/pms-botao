/* eslint-disable no-undef */
import { shallowMount } from '@vue/test-utils'
import PmsBotao from '@/pms-botao.vue'

let wrapper
let mount = true

let propsData = {
  reverso: false,
  estilo: 'secundario',
  icone: 'check',
  target: 'foo',
  toggle: 'bar',
  rotulo: 'Rótulo #1'
}

beforeEach(() => {
  if (!mount) return false
  wrapper = shallowMount(PmsBotao, { propsData })
})

describe('pms-botao.vue', () => {
  it(`é do tipo 'button'`, () => {
    expect(wrapper.find('button').attributes('type')).toEqual('button')
  })

  it(`possui estilo definido`, () => {
    expect(wrapper.classes()).toContain('btn-primary')
  })

  it(`possui ícone definido`, () => {
    expect(wrapper.find('.fa').classes()).toEqual(['fa', 'fa-check'])
    propsData.reverso = true
  })

  it('possui ícone à direita', () => {
    expect(wrapper.find('.btn').classes()).toContain('reverso')
  })

  it('possui rótulo definido', () => {
    expect(wrapper.find('button').text()).toEqual(propsData.rotulo)
  })

  it(`possui atributo 'target' definido`, () => {
    expect(wrapper.find('button').attributes('data-target')).toBe(propsData.target)
  })

  it(`possui atributo 'toggle' definido`, () => {
    expect(wrapper.find('button').attributes('data-toggle')).toBe(propsData.toggle)
    propsData.estilo = '-invalido-'
    mount = false
  })

  it('aciona erro ao receber estilo inválido', () => {
    const spy = jest
      .spyOn(global.console, 'error')
      .mockImplementation(() => {})

    try {
      shallowMount(PmsBotao, { propsData })
    } catch (Error) {
      const mensagem = `O Estilo de botão '${propsData.estilo}' não existe`
      expect(Error.message).toBe(mensagem)
    } finally {
      spy.mockRestore()
    }
    
    propsData.estilo = 'secundario'
    mount = true
  })

  it('emite evento ao ser clicado', () => {
    const botao = wrapper.find('button')
    botao.trigger('click')
    expect(wrapper.emitted('clicado')).toBeTruthy()
  })
})
