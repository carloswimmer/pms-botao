var props = {
  tipo: {
    type: String,
    required: false,
    default: 'button'
  },
  
  estilo: {
    type: String,
    required: false,
    default: 'primario'
  },
  
  rotulo: {
    type: String,
    required: false,
    default: ''
  },
  
  icone: {
    type: String,
    required: false,
    default: ''
  },
  
  reverso: {
    type: Boolean,
    required: false,
    default: false
  },
  
  toggle: {
    default: '',
    type: String,
    required: false
  },
  
  target: {
    default: '',
    type: String,
    required: false
  },
  
  dismiss: {
    default: '',
    type: String,
    required: false
  }
};

var estilos = [
  { nome: 'secundario', classes: 'btn-primary' },
  { nome: 'primario', classes: 'btn-primary side-btn-primary' },
  { nome: 'secundario-leve', classes: 'btn-default' },
  { nome: 'desfaz', classes: 'btn-default side-btn-default' },
  { nome: 'secundario-dir', classes: 'btn-primary btn-next' },
  { nome: 'primario-dir', classes: 'btn-primary side-btn-primary btn-next' },
  { nome: 'secundario-leve-dir', classes: 'btn-default btn-next' },
  { nome: 'secundario-esq', classes: 'btn-primary btn-previous' },
  { nome: 'secundario-leve-esq', classes: 'btn-default btn-previous' },
  { nome: 'desfaz-esq', classes: 'btn-default side-btn-default btn-previous' },
  { nome: 'primario-esq', classes: 'btn-primary side-btn-primary btn-previous' },
  { nome: 'atencao-esq', classes: 'btn-warning btn-previous' },
  { nome: 'secundario-sm', classes: 'btn-primary btn-sm' },
  { nome: 'atencao-sm', classes: 'btn-warning btn-sm' },
  { nome: 'perigo-sm', classes: 'btn-danger btn-sm' },
  { nome: 'secundario-leve-sm', classes: 'btn-default btn-sm' },
  { nome: 'primario-lg', classes: 'btn-primary side-btn-primary btn-subscribe btn-lg' },
  { nome: 'secundario-lg', classes: 'btn-primary btn-subscribe btn-lg' },
  { nome: 'secundario-inline', classes: 'btn-primary btn-inline' }
];

var icones = [
  { nome: 'check', classes: 'fa-check' },
  { nome: 'ban', classes: 'fa-ban' },
  { nome: 'file', classes: 'fa-file-o' },
  { nome: 'plus', classes: 'fa-plus' },
  { nome: 'floppy', classes: 'fa-floppy-o' },
  { nome: 'eraser', classes: 'fa-eraser' },
  { nome: 'trash', classes: 'fa-trash fa-lg' },
  { nome: 'pencil', classes: 'fa-pencil fa-lg' },
  { nome: 'print', classes: 'fa fa-print fa-lg' },
  { nome: 'caret-left', classes: 'fa-caret-left fa-lg' },
  { nome: 'caret-right', classes: 'fa-caret-right fa-lg' },
  { nome: 'search', classes: 'fa-search' },
  { nome: 'eye', classes: 'fa-eye fa-lg' },
  { nome: 'spinner', classes: 'fa-spinner fa-pulse' },
  { nome: 'power-off', classes: 'fa-power-off' }
];

//

var script = {
  name: 'PmsBotao',
  props: props,
  
  computed: {
    classesEstilo: function classesEstilo () {
      return this.classes() || ''
    },

    classesIcone: function classesIcone () {
      if (!this.icone) { return '' }
      return this.classes(false) || ''
    },
    
    modo: function modo () {
      return this.reverso ? 'reverso' : ''
    },
  },
  
  methods: {
    dispara: function dispara () {
      this.$emit('clicado');
      var botao = document.querySelector('.btn');
      if (botao) { botao.blur(); }
    },

    classes: function classes (estilo) {
      var this$1 = this;
      if ( estilo === void 0 ) estilo = true;

      if (!this.estilo && estilo) {
        throw new Error("O estilo não pode estar vazio")
      }

      var retorno = estilo
        ? estilos.find(function (estilo) { return estilo.nome == this$1.estilo; })
        : icones.find(function (icone) { return icone.nome == this$1.icone; });

      if (!retorno) {
        var mensagem = estilo ? ['Estilo', this.estilo] : ['Ícone', this.icone];
        throw new Error(("O " + (mensagem[0]) + " de botão '" + (mensagem[1]) + "' não existe"))
      }

      return retorno.classes
    }
  }
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
/* server only */
, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
  if (typeof shadowMode !== 'boolean') {
    createInjectorSSR = createInjector;
    createInjector = shadowMode;
    shadowMode = false;
  } // Vue.extend constructor export interop.


  var options = typeof script === 'function' ? script.options : script; // render functions

  if (template && template.render) {
    options.render = template.render;
    options.staticRenderFns = template.staticRenderFns;
    options._compiled = true; // functional template

    if (isFunctionalTemplate) {
      options.functional = true;
    }
  } // scopedId


  if (scopeId) {
    options._scopeId = scopeId;
  }

  var hook;

  if (moduleIdentifier) {
    // server build
    hook = function hook(context) {
      // 2.3 injection
      context = context || // cached call
      this.$vnode && this.$vnode.ssrContext || // stateful
      this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
      // 2.2 with runInNewContext: true

      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__;
      } // inject component styles


      if (style) {
        style.call(this, createInjectorSSR(context));
      } // register component module identifier for async chunk inference


      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier);
      }
    }; // used by ssr in case component is cached and beforeCreate
    // never gets called


    options._ssrRegister = hook;
  } else if (style) {
    hook = shadowMode ? function () {
      style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
    } : function (context) {
      style.call(this, createInjector(context));
    };
  }

  if (hook) {
    if (options.functional) {
      // register for functional component in vue file
      var originalRender = options.render;

      options.render = function renderWithStyleInjection(h, context) {
        hook.call(context);
        return originalRender(h, context);
      };
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate;
      options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
    }
  }

  return script;
}

var normalizeComponent_1 = normalizeComponent;

var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
  return function (id, style) {
    return addStyle(id, style);
  };
}
var HEAD;
var styles = {};

function addStyle(id, css) {
  var group = isOldIE ? css.media || 'default' : id;
  var style = styles[group] || (styles[group] = {
    ids: new Set(),
    styles: []
  });

  if (!style.ids.has(id)) {
    style.ids.add(id);
    var code = css.source;

    if (css.map) {
      // https://developer.chrome.com/devtools/docs/javascript-debugging
      // this makes source maps inside style tags work properly in Chrome
      code += '\n/*# sourceURL=' + css.map.sources[0] + ' */'; // http://stackoverflow.com/a/26603875

      code += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) + ' */';
    }

    if (!style.element) {
      style.element = document.createElement('style');
      style.element.type = 'text/css';
      if (css.media) { style.element.setAttribute('media', css.media); }

      if (HEAD === undefined) {
        HEAD = document.head || document.getElementsByTagName('head')[0];
      }

      HEAD.appendChild(style.element);
    }

    if ('styleSheet' in style.element) {
      style.styles.push(code);
      style.element.styleSheet.cssText = style.styles.filter(Boolean).join('\n');
    } else {
      var index = style.ids.size - 1;
      var textNode = document.createTextNode(code);
      var nodes = style.element.childNodes;
      if (nodes[index]) { style.element.removeChild(nodes[index]); }
      if (nodes.length) { style.element.insertBefore(textNode, nodes[index]); }else { style.element.appendChild(textNode); }
    }
  }
}

var browser = createInjector;

/* script */
var __vue_script__ = script;

/* template */
var __vue_render__ = function() {
  var _vm = this;
  var _h = _vm.$createElement;
  var _c = _vm._self._c || _h;
  return _c(
    "button",
    {
      class: "btn " + _vm.classesEstilo + " " + _vm.modo,
      attrs: {
        "data-pms": "botao",
        type: _vm.tipo,
        "data-toggle": _vm.toggle,
        "data-target": _vm.target,
        "data-dismiss": _vm.dismiss
      },
      on: {
        click: function($event) {
          return _vm.dispara()
        }
      }
    },
    [
      _vm.icone ? _c("i", { class: "fa " + _vm.classesIcone }) : _vm._e(),
      _vm._v(" "),
      _vm.rotulo
        ? _c("span", { staticClass: "rotulo" }, [
            _vm._v(_vm._s(_vm.rotulo) + "\n  ")
          ])
        : _vm._e()
    ]
  )
};
var __vue_staticRenderFns__ = [];
__vue_render__._withStripped = true;

  /* style */
  var __vue_inject_styles__ = function (inject) {
    if (!inject) { return }
    inject("data-v-ed88dad0_0", { source: "[data-pms='botao'].btn {\n  display: flex;\n  align-items: center;\n  justify-content: space-around;\n}\n[data-pms='botao'].reverso {\n  flex-direction: row-reverse;\n}\n[data-pms='botao'] .fa {\n  margin: 0 0.2rem;\n}\n", map: undefined, media: undefined });

  };
  /* scoped */
  var __vue_scope_id__ = undefined;
  /* module identifier */
  var __vue_module_identifier__ = undefined;
  /* functional template */
  var __vue_is_functional_template__ = false;
  /* style inject SSR */
  

  
  var pmsBotao = normalizeComponent_1(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    browser,
    undefined
  );

export default pmsBotao;
