# pms-button

Componente de botão para aplicações Vue.

## Instalação

```bash
npm install git+https://dev.santos.sp.gov.br/publico/pms-botao.git#{VERSAO} --save
```

## Props

| Obrigatório | Nome | Tipo | Default | Formato |
|-------------|------|------|---------|---------|
| Não | estilo | `String` | 'primario' | `lowercase` |
| Não | tipo | `String` | 'button' | `lowercase` |
| Não | rotulo | `String` | '' (vazio) | livre |
| Não | icone | `String` | '' (vazio) | `kebab-case` |
| Não | reverso (1) | `Boolean` | false | |
| Não | toggle | `String` | '' (vazio) | `lowercase` |
| Não | target | `String` | '' (vazio) | `lowercase` |
| Não | dismiss | `String` | '' (vazio) | `lowercase` |

**Referências:**

[**- Ícones e estilos disponíveis**](https://dev.santos.sp.gov.br/publico/pms-botao/blob/master/src/config/botao.js)<br>
[**- Arquivo de definição das props**](https://dev.santos.sp.gov.br/publico/pms-botao/blob/master/src/config/botao.js)

**(1) Observação:**<br>

A função da *prop* **reverso** é posicionar os elementos que se encontram dentro do botão, ou seja, o ícone (se houver) e o rótulo do botão.

Por estar definido como `false`, o **reverso** manterá o ícone à direita e o texto, consequentemente, à esquerda. Para obter o efeito contrário, defina-o com o `true`.

Por ser do tipo `Boolean`, esta *prop* pode ser definida da seguinte forma:

```html
<pms-botao
  reverso
  icone="check"
  estilo="primario"
  rotulo="Botão primário"
  @clicado="clique"
/>
```

No exemplo acima, a *prop* está sendo enviada com valor `true`. A não inclusão da *prop* resulta no valor *default* `false`.

## Evento 'click'

O componente em questão possui um elemento `button`, que, uma vez clicado, efetuará o evento customizado `clicado`. Com isto em mente, tudo o que você precisará definir é o mecanismo de escuta deste evento disparado.

O primeiro passo é incluir a escuta deste evento na chamada do componente dentro da sua `tag` template:<br>

```html
<pms-botao @clicado="clique"/>
```

Feito isto, restará apenas a criação do método inserido anteriormente como `callback` ao evento efetuado:<br>

```javascript
clique() {
  // ...
}
```